import {createApp} from "vue";
import App from "./App.vue";
import router from "./router";
import {Auth0} from "@/auth";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faLink, faUser, faPowerOff } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

async function init() {
    const AuthPlugin = await Auth0.init({
        onRedirectCallback: (appState) => {
            router.push(
                appState && appState.targetUrl
                    ? appState.targetUrl
                    : window.location.pathname,
            )
        },
        clientId: 'uyAzuH0jEfABDcRw3IAeGCcG2ltbaBIF',
        domain: 'dev-nv0e8oyc.eu.auth0.com',
        audience: '',
        redirectUri: window.location.origin,
    });
    const app = createApp(App);
    library.add(faLink, faUser, faPowerOff);
    app
        .use(AuthPlugin)
        .use(router)
        .component("font-awesome-icon", FontAwesomeIcon)
        .mount('#app');
}

init();
